/*

  call, bind, apply

*/


// Попробуем вызвать функцию, в которой отсутсвуют переменные
function add(c, d) {
  console.log(this);
  console.log(this.a + this.b + c + d);
}

// add(3,4); //NaN

// https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Function/call
// Создадим новый обьект который подключим к нашей функции через метод .call
// Метод call() вызывает функцию с указанным значением this и индивидуально предоставленными аргументами.
// Syntax: fun.call(thisArg[, arg1[, arg2[, ...]]])
// var ten = {a: 1, b: 2 };
//
// add.call( ten, 3, 4 ); // 10

// https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Function/apply
// Метод apply() вызывает функцию с указанным значением this и аргументами, предоставленными в виде массива

// var ArrayArguments = [1,2];
// add.apply(ten, ArrayArguments);

  // function Product(name, price) {
  //   this.name = name;
  //   this.price = price;
  //
  //   if (price < 0) {
  //     throw RangeError('Cannot create product ' + this.name + ' with a negative price');
  //   }
  // }
  //
  // // Конструктор Food
  // function Food(name, price) {
  //   Product.call(this, name, price);
  //   this.category = 'food';
  // }
  //
  // // Конструктор Toy
  // function Toy(name, price) {
  //   Product.call(this, name, price);
  //   this.category = 'toy';
  // }
  //
  // var cheese = new Food('feta', 5);
  // var fun = new Toy('robot', 40);
  // console.log( fun, cheese );

  // bind
  // Метод bind() создаёт новую функцию, которая при вызове устанавливает в
  // качестве контекста выполнения this предоставленное значение
  //
  // function saySomething(event){
  //   console.log( 'saySomething:', this.something, this, event );
  // }
  // saySomething();
  // var cat = { something: 'meow' };
  // var CatSays = saySomething.bind(cat);
  //     CatSays();

  // Проверим на другом примере
  // var sms = document.getElementById('sms');
  //
  //     sms.addEventListener('click', saySomething);
  //     sms.addEventListener('click', CatSays);

    /*

      Задание 1.

      Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
      значение из this.color, this.background
      А так же добавляет элемент h1 с текстом "I know how binding works in JS"

      1.1 Ф-я принимает один аргумент, второй попадает в него через метод .call(obj)
      1.2 Ф-я не принимает никаких аргументов, а необходимые настройки полностью передаются через bind
      1.3 Ф-я принимает фразу для заголовка, обьект с настройками передаем через .apply();


    */

    var settings = {
        color: "red",
        backgroundColor: "blue"
    }

    function changeColor1(backgroundColor) {
        document.body.style.backgroundColor = backgroundColor;
        document.body.style.color = this.color;
    };

    function changeColor2(backgroundColor, color) {
        document.body.style.backgroundColor = backgroundColor;
        document.body.style.color = color;
    };

    function changeColor3(phrase) {
        document.body.style.backgroundColor = this.backgroundColor;
        document.body.style.color = this.color;
        document.body.innerHTML = phrase;
    }

    // changeColor1.call(settings, 'red');
    // changeColor2.call('green','blue');
    // changeColor3.call(settings,['I know how binding works in JS']);

/*

  Задание:

  Написать функцию генератор, которая будет иметь приватные и публичные свойства.
  Публичные методы должны вызывать приватные.

  Рассмотрим на примере планеты:

    - На вход принимаются параметр Имя планеты.

    Создается новый обьект, который имеет публичные методы и свойства:
    - name (передается через конструктор)
    - population ( randomPopulation());
    - rotatePlanet(){
      let randomNumber = Math.floor(Math.random() * (1000 - 1 + 1)) + 1;
      if ( (randomNumber % 2) == 0) {
        growPopulation(state);
      } else {
        Cataclysm(state);
      }
    }

    Приватные методы
    randomPopulation -> Возвращает случайное целое число от 1.000 до 10.000
    growPopulation() {
      функция которая берет приватное свойство populationMultiplyRate
      которое равняется случайному числу от 1 до 10 и умножает его на 100.
      Дальше, число которое вышло добавляет к популяции и выводит в консоль сообщение,
      что за один цикл прибавилось столько населения на планете .
    }
    Cataclysm(){
      Рандомим число от 1 до 10 и умножаем его на 10000;
      То число которое получили, отнимаем от популяции.
      В консоль выводим сообщение - от катаклизма погибло Х человек на планете.
    }


*/

function planetGenerator(name){
    this.name = name;
    this.population = randomPopulation();
    this.rotatePlanet = function () {
        var randomNumber = Math.floor(Math.random() * (10000 - 1 + 1)) + 1;
        if ( (randomNumber % 2) == 0) {
            growPopulation.call(this);
        } else {
            cataclysm.call(this);
        }
    };

    function randomPopulation() {
        return Math.floor(Math.random() * (1000 - 1 + 1)) + 1;
    };
    function growPopulation() {
        this.population = this.population + 1000;
        console.log(this.population);
    };
    function cataclysm() {
        this.population = this.population - 1000;
        if(this.population < 0){
            this.population = 8;
        }
        console.log(this.population);
    };

};

var mars = new planetGenerator('Mars');
mars.rotatePlanet();